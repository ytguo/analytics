// Databricks notebook source
// STARTER CODE - DO NOT EDIT THIS CELL
import org.apache.spark.sql.functions.desc
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import spark.implicits._
import org.apache.spark.sql.expressions.Window

// COMMAND ----------

// STARTER CODE - DO NOT EDIT THIS CELL
val customSchema = StructType(Array(StructField("lpep_pickup_datetime", StringType, true), StructField("lpep_dropoff_datetime", StringType, true), StructField("PULocationID", IntegerType, true), StructField("DOLocationID", IntegerType, true), StructField("passenger_count", IntegerType, true), StructField("trip_distance", FloatType, true), StructField("fare_amount", FloatType, true), StructField("payment_type", IntegerType, true)))

// COMMAND ----------

// STARTER CODE - YOU CAN LOAD ANY FILE WITH A SIMILAR SYNTAX.
val df = spark.read
   .format("com.databricks.spark.csv")
   .option("header", "true") // Use first line of all files as header
   .option("nullValue", "null")
   .schema(customSchema)
   .load("/FileStore/tables/nyc_tripdata.csv") // the csv file which you want to work with
   .withColumn("pickup_datetime", from_unixtime(unix_timestamp(col("lpep_pickup_datetime"), "MM/dd/yyyy HH:mm")))
   .withColumn("dropoff_datetime", from_unixtime(unix_timestamp(col("lpep_dropoff_datetime"), "MM/dd/yyyy HH:mm")))
   .drop($"lpep_pickup_datetime")
   .drop($"lpep_dropoff_datetime")

// COMMAND ----------

// LOAD THE "taxi_zone_lookup.csv" FILE SIMILARLY AS ABOVE. CAST ANY COLUMN TO APPROPRIATE DATA TYPE IF NECESSARY.

// ENTER THE CODE BELOW
//create custom schema structure
val taxi_zone_schema = StructType(Array(StructField("LocationID", IntegerType, true), StructField("Borough", StringType, true), StructField("Zone", StringType, true), StructField("service_zone", StringType, true)))
//create dataframe
val taxi_zone_lookup = spark.read
   .format("com.databricks.spark.csv")
   .option("header", "true") // Use first line of all files as header
   .option("nullValue", "null")
   .schema(taxi_zone_schema)
   .load("/FileStore/tables/taxi_zone_lookup.csv") // the csv file which you want to work with
//print schema of dataframe taxi_zone_lookup
taxi_zone_lookup.printSchema()

// COMMAND ----------

// STARTER CODE - DO NOT EDIT THIS CELL
// Some commands that you can use to see your dataframes and results of the operations. You can comment the df.show(5) and uncomment display(df) to see the data differently. You will find these two functions useful in reporting your results.
// display(df)
df.show(5) // view the first 5 rows of the dataframe

// COMMAND ----------

// STARTER CODE - DO NOT EDIT THIS CELL
// Filter the data to only keep the rows where "PULocationID" and the "DOLocationID" are different and the "trip_distance" is strictly greater than 2.0 (>2.0).

// VERY VERY IMPORTANT: ALL THE SUBSEQUENT OPERATIONS MUST BE PERFORMED ON THIS FILTERED DATA

val df_filter = df.filter($"PULocationID" =!= $"DOLocationID" && $"trip_distance" > 2.0)
df_filter.show(5)

// COMMAND ----------

// PART 1a: The top-5 most popular drop locations - "DOLocationID", sorted in descending order - if there is a tie, then one with lower "DOLocationID" gets listed first
// Output Schema: DOLocationID int, number_of_dropoffs int 

// Hint: Checkout the groupBy(), orderBy() and count() functions.

// ENTER THE CODE BELOW
val df_DO = df_filter.groupBy("DOLocationID").agg(count("*").as("number_of_dropoffs")).orderBy($"number_of_dropoffs".desc, $"DOLocationID".asc).withColumn("number_of_dropoffs", col("number_of_dropoffs").cast("int")).select("DOLocationID", "number_of_dropoffs")

df_DO.show(5)

// COMMAND ----------

// PART 1b: The top-5 most popular pickup locations - "PULocationID", sorted in descending order - if there is a tie, then one with lower "PULocationID" gets listed first 
// Output Schema: PULocationID int, number_of_pickups int

// Hint: Code is very similar to part 1a above.

// ENTER THE CODE BELOW
val df_PU = df_filter.groupBy("PULocationID").agg(count("*").alias("number_of_pickups")).orderBy($"number_of_pickups".desc, $"PULocationID".asc).withColumn("number_of_pickups", col("number_of_pickups").cast("int")).select("PULocationID", "number_of_pickups")

df_PU.show(5)

// COMMAND ----------

// PART 2: List the top-3 locations with the maximum overall activity, i.e. sum of all pickups and all dropoffs at that LocationID. In case of a tie, the lower LocationID gets listed first.
// Output Schema: LocationID int, number_activities int

// Hint: In order to get the result, you may need to perform a join operation between the two dataframes that you created in earlier parts (to come up with the sum of the number of pickups and dropoffs on each location). 
// ENTER THE CODE BELOW
val df_top_loc = df_PU.join(df_DO, df_DO("DOLocationID") === df_PU("PULocationID"), "inner").withColumn("number_activities", col("number_of_pickups")+ col("number_of_dropoffs")).withColumn("LocationID", col("DOLocationID")).orderBy($"number_activities".desc, $"LocationID".asc).select("LocationID", "number_activities")

df_top_loc.show(3)


// COMMAND ----------

// PART 3: List all the boroughs in the order of having the highest to lowest number of activities (i.e. sum of all pickups and all dropoffs at that LocationID), along with the total number of activity counts for each borough in NYC during that entire period of time.
// Output Schema: Borough string, total_number_activities int

// Hint: You can use the dataframe obtained from the previous part, and will need to do the join with the 'taxi_zone_lookup' dataframe. Also, checkout the "agg" function applied to a grouped dataframe.

// ENTER THE CODE BELOW
val df_top_boroughs = df_top_loc.join(taxi_zone_lookup, df_top_loc("LocationID") === taxi_zone_lookup("LocationID"), "inner").groupBy("Borough").agg(sum("number_activities").alias("total_number_activities")).orderBy($"total_number_activities".desc) //order by highest to lowest - AKA desc total number of activities

//show all the boroughs
df_top_boroughs.show()


// COMMAND ----------

// PART 4: List the top 2 days of week with the largest number of (daily) average pickups, along with the values of average number of pickups on each of the two days. The day of week should be a string with its full name, for example, "Monday" - not a number 1 or "Mon" instead.
// Output Schema: day_of_week string, avg_count float

// Hint: You may need to group by the "date" (without time stamp - time in the day) first. Checkout "to_date" function.

// ENTER THE CODE BELOW

//first aggregate by the different dates (not days) to get count of pickups by dates
//change pickup_date to strings
val df_top_PU_dates = df_filter.withColumn("day_of_week", date_format(col("pickup_datetime"), "EEEE").cast("string")).withColumn("pickup_datetime", to_date(col("pickup_datetime"),"yyyy-MM-dd")).groupBy("day_of_week","pickup_datetime").agg(count("*").alias("daily_pick_up"))

//second, aggregate dates by the day (Monday, Tuesday, etc.) and divide by the count of days to get the average pickups per day
val df_top_PU_days = df_top_PU_dates.groupBy("day_of_week").agg(sum("daily_pick_up").alias("total_pickups"), count("*").alias("count")).withColumn("avg_count", (col("total_pickups")/col("count")).cast("float")).orderBy($"avg_count".desc).select("day_of_week", "avg_count") //order by highest to lowest avg_count and change avg_count to floats

//show top 2 days
df_top_PU_days.show(2)


// COMMAND ----------

// PART 5: For each particular hour of a day (0 to 23, 0 being midnight) - in their order from 0 to 23, find the zone in Brooklyn borough with the LARGEST number of pickups. 
// Output Schema: hour_of_day int, zone string, max_count int

// Hint: You may need to use "Window" over hour of day, along with "group by" to find the MAXIMUM count of pickups

// ENTER THE CODE BELOW
//filter for only Brooklyn entries then group by the pickup hour and the zone 
val df_count_zone = taxi_zone_lookup.filter($"Borough" === "Brooklyn").join(df_filter, df_filter("PULocationID") === taxi_zone_lookup("LocationID"), "left").withColumn("hour_of_day", hour(col("pickup_datetime"))).groupBy("hour_of_day", "Zone").agg(count("*").alias("PU_count")).withColumn("PU_count", col("PU_count").cast("integer"))

val df_max_count = df_count_zone.groupBy("hour_of_day").agg(max("PU_count").alias("max_count")).withColumn("max_count", col("max_count").cast("integer")).select("hour_of_day", "max_count")

//Instead of using "Windows", join the 2 dataframes df_count_zone & df_max_count. Inner join on the hour of day and when the pickup count = max count for the hour
val df_top_zone = df_max_count.join(df_count_zone, (df_max_count("max_count") === df_count_zone("PU_count")) && (df_max_count("hour_of_day") === df_count_zone("hour_of_day")), "inner").drop(df_count_zone("hour_of_day")).drop(df_count_zone("PU_count")).orderBy(df_count_zone("hour_of_day")) //keep the order of the hours and drop unnecessary columns

df_top_zone.select("hour_of_day", "Zone", "max_count").show(24)

// COMMAND ----------

// PART 6 - Find which 3 different days of the January, in Manhattan, saw the largest percentage increment in pickups compared to previous day, in the order from largest increment % to smallest increment %. 
// Print the day of month along with the percent CHANGE (can be negative), rounded to 2 decimal places, in number of pickups compared to previous day.
// Output Schema: day int, percent_change float


// Hint: You might need to use lag function, over a window ordered by day of month.

// ENTER THE CODE BELOW
// Filter for Manhattan, account for only January dates (based on Piazza post, Jan 1st change will NOT be accounted for)
val previous_day_pickup = taxi_zone_lookup.filter($"Borough" === "Manhattan").join(df_filter, df_filter("PULocationID") === taxi_zone_lookup("LocationID"), "inner").withColumn("pickup_datetime", to_date(col("pickup_datetime"),"yyyy-MM-dd")).filter((date_format(col("pickup_datetime"), "MMMM") === "January")).groupBy(col("pickup_datetime")).agg(count("*").alias("PU_count"))

//use lag to find the previous day count and the previous date
val window = Window.orderBy("pickup_datetime")
val previous_count = lag(col("PU_count"), 1).over(window)
val prevous_date = lag(col("pickup_datetime"), 1).over(window)

//calculate % change from the previous day pickup and cast as float, order by the largest change (using absolute value)
//show only the day in January and cast as int
val highest_day_change = previous_day_pickup.withColumn("previous_day_pickup", previous_count).withColumn("prevous_date", prevous_date).filter(date_format(col("pickup_datetime"), "YYYY") === date_format(col("prevous_date"), "YYYY")).withColumn("day", date_format(col("pickup_datetime"), "dd").cast("int")).withColumn("percent_change", bround(($"PU_count" - $"previous_day_pickup")*100/$"previous_day_pickup", 2).cast("float")).orderBy($"pickup_datetime").orderBy(abs($"percent_change").desc).select("day", "percent_change")

//filter to see only top 3 days
highest_day_change.show(3)
