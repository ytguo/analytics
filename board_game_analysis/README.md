# Board Game Data Analysis

# Overview
This project aims to create intuitive visualizations that depict board game data through the use of JavaScript (D3) and Tableau. There are 5 main portions:

1. Bar graphs created in Tableau to analyze board games by popularity

2. Undirecteed network graph to analyze similarities between board games

3. Line charts for board game ratings from 2016 - 2020

4. Interactive line graph with mouse-over bar chats for board game ratings year over year

5. Choropleth map of overall board game ratings by country

# Setup
1. Download and extract the project code
2. Run `python -m http.server 8000`